const express = require('express');
const router = express.Router();
const User = require('../models/user');
const auth = require('../middleware/auth');

// Маршрут для отримання інформації про поточного користувача
// Маршрут для отримання інформації про поточного користувача
router.get('/users/me', auth, async (req, res) => {
    await req.user.populate('tasks');
    res.send(req.user);
});


// Реєстрація нового користувача
router.post('/users/register', async (req, res) => {
    const userData = req.body;
    try {
        const newUser = new User(userData);
        await newUser.save();
        const token = await newUser.generateAuthToken();
        res.status(201).send({ user: newUser, token });
    } catch (error) {
        res.status(400).send(error.message);
    }
});

// Вхід користувача
router.post("/users/login", async (req, res) => {
    try {
        const user = await User.findByCredentials(req.body.email, req.body.password);
        const token = await user.generateAuthToken();
        res.send({ user, token });
    } catch (e) {
        res.status(400).send(e.message);
    }
});

// Вихід користувача
router.post('/users/logout', auth, async (req, res) => {
    try {
        req.user.tokens = req.user.tokens.filter((token) => token.token !== req.token);
        await req.user.save();
        res.send('Successfully logged out');
    } catch (error) {
        res.status(500).send('Internal Server Error');
    }
});

// Вихід користувача з усіх сесій
router.post('/users/logoutAll', auth, async (req, res) => {
    try {
        req.user.tokens = [];
        await req.user.save();
        res.send('Successfully logged out from all devices');
    } catch (error) {
        res.status(500).send('Internal Server Error');
    }
});


// Маршрут для отримання всіх користувачів
router.get("/users", (req, res) => {
    // Знайти всіх користувачів у базі даних
    User.find({})
        .then((users) => {
            // Відправити користувачів у відповідь як список
            res.status(200).send(users);
        })
        .catch((error) => {
            // Відправити статус помилки 500, якщо виникла помилка при обробці запиту
            res.status(500).send('Internal Server Error');
        });
});

// Маршрут для отримання користувача за його id
router.get('/users/:id', (req, res) => {
    const userId = req.params.id;
    // Знайти користувача за його id у базі даних
    User.findById(userId)
        .then(user => {
            if (!user) {
                // Якщо користувача не знайдено, відправити повідомлення про помилку
                return res.status(404).json({ message: 'User not found' });
            }
            // Відправити знайденого користувача у відповідь
            res.status(200).json(user);
        })
        .catch(error => {
            // Відправити статус помилки 500, якщо виникла помилка при обробці запиту
            console.error(error);
            res.status(500).send('Internal Server Error');
        });
});


// Маршрут для видалення користувача за його id
router.delete('/user/:id', async (req, res) => {
    const userId = req.params.id;

    try {
        // Знайти та видалити користувача за його id у базі даних
        const user = await User.findByIdAndDelete(userId);

        if (!user) {
            // Якщо користувача не знайдено, відправити повідомлення про помилку
            return res.status(404).send("User not found");
        }

        // Відправити підтвердження про успішне видалення користувача
        res.status(200).send("User deleted successfully");
    } catch (error) {
        // Відправити статус помилки 400, якщо виникла помилка при видаленні користувача
        console.error(error);
        res.status(400).send("Error deleting user");
    }
});

// Маршрут для видалення всіх користувачів
router.delete('/users', async (req, res) => {
    try {
        // Видалити всіх користувачів з бази даних
        await User.deleteMany({});
        // Відправити підтвердження про успішне видалення всіх користувачів
        res.status(200).send("All users deleted successfully");
    } catch (error) {
        // Відправити статус помилки 400, якщо виникла помилка при видаленні користувачів
        console.error(error);
        res.status(400).send("Error deleting users");
    }
});


// Тестовий маршрут
router.get('/test', (req, res) => {
    // Відправити повідомлення "From a new File" у відповідь
    res.send("From a new File");
});

// Маршрут для оновлення даних користувача за його id
router.patch("/users/:id", async (req, res) => {
    try {
        const userId = req.params.id;
        const updates = req.body;

        // Знайти користувача за його id у базі даних
        const user = await User.findById(userId);

        if (!user) {
            // Якщо користувача не знайдено, відправити повідомлення про помилку
            return res.status(404).send("User not found");
        }

        // Оновити дані користувача з отриманими від клієнта оновленнями
        Object.keys(updates).forEach((field) => {
            user[field] = updates[field];
        });

        // Зберегти оновленого користувача у базі даних
        await user.save();

        // Відправити оновленого користувача у відповідь
        res.status(200).send(user);
    } catch (error) {
        // Відправити статус помилки 500, якщо виникла помилка при обробці запиту
        console.error(error);
        res.status(500).send("Internal Server Error");
    }
});

module.exports = router;