const express = require('express');
const router = express.Router();
const Task = require('../models/task');
const auth = require('../middleware/auth');

// POST для додавання нового завдання
router.post('/tasks', auth, async (req, res) => {
    const task = new Task({
        ...req.body,
        owner: req.user._id
    });

    try {
        await task.save();
        res.status(201).send(task);
    } catch (e) {
        res.status(400).send(e);
    }
});

// GET для отримання задачі за ID
router.get('/tasks/:id', auth, async (req, res) => {
    const _id = req.params.id;

    try {
        const task = await Task.findOne({ _id, owner: req.user._id }).populate('owner');
        if (!task) {
            return res.status(404).send({ error: 'Task not found' });
        }
        res.send(task);
    } catch (e) {
        res.status(500).send({ error: 'Server error' });
    }
});

// GET для отримання всіх задач користувача
router.get('/tasks', auth, async (req, res) => {
    try {
        await req.user.populate('tasks');
        res.send(req.user.tasks);
    } catch (e) {
        res.status(500).send({ error: 'Server error' });
    }
});

// PATCH для оновлення задачі
router.patch('/tasks/:id', auth, async (req, res) => {
    const updates = Object.keys(req.body);
    const allowedUpdates = ['description', 'completed'];
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update));

    if (!isValidOperation) {
        return res.status(400).send({ error: 'Invalid updates!' });
    }

    try {
        const task = await Task.findOne({ _id: req.params.id, owner: req.user._id });

        if (!task) {
            return res.status(404).send({ error: 'Task not found' });
        }

        updates.forEach((update) => task[update] = req.body[update]);
        await task.save();
        res.send(task);
    } catch (e) {
        res.status(400).send({ error: 'Update failed' });
    }
});

// DELETE для видалення задачі
router.delete('/tasks/:id', auth, async (req, res) => {
    try {
        const task = await Task.findOneAndDelete({ _id: req.params.id, owner: req.user._id });

        if (!task) {
            return res.status(404).send({ error: 'Task not found' });
        }

        res.send({
            message: 'Task successfully deleted',
            task: task
        });
    } catch (e) {
        res.status(500).send({ error: 'Server error' });
    }
});

// DELETE для видалення всіх задач користувача
router.delete('/tasks', auth, async (req, res) => {
    try {
        const tasks = await Task.deleteMany({ owner: req.user._id });

        if (tasks.deletedCount === 0) {
            return res.status(404).send({ error: 'No tasks found for this user' });
        }

        res.send({
            message: 'All tasks successfully deleted',
            tasksDeletedCount: tasks.deletedCount
        });
    } catch (e) {
        res.status(500).send({ error: 'Server error' });
    }
});


module.exports = router;